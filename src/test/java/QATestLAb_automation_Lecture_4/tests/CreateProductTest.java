package QATestLAb_automation_Lecture_4.tests;

import QATestLAb_automation_Lecture_4.BaseTest;
import QATestLAb_automation_Lecture_4.model.DataProviderClass;
import QATestLAb_automation_Lecture_4.model.ProductData;
import org.testng.annotations.Test;

public class CreateProductTest extends BaseTest {

    private ProductData newProductData;

    @Test(dependsOnMethods={"createNewProduct"})
    public void checkNewProduct() {
        // TODO implement logic to check product visibility on website
        try {
            actions.checkProduct(newProductData);
        } catch (UnsupportedOperationException e) {}
    }

    @Test(dataProvider = "data-provider", dataProviderClass = DataProviderClass.class)
    public void createNewProduct(String login, String password) {
        // TODO implement test for product creation
        try {
            actions.login(login, password);
        } catch (UnsupportedOperationException e) {}

        newProductData = ProductData.generate();

        try {
            actions.createProduct(newProductData);
        } catch (UnsupportedOperationException e) {}
        try {
            actions.logout();
        } catch (UnsupportedOperationException e) {}
    }
}
