package QATestLAb_automation_Lecture_4;

import QATestLAb_automation_Lecture_4.model.ProductData;
import QATestLAb_automation_Lecture_4.utils.Properties;
import QATestLAb_automation_Lecture_4.utils.logging.CustomReporter;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

/**Contains main script actions that may be used in scripts.*/
public class GeneralActions {
    private WebDriver driver;
    private WebDriverWait wait;
    private Actions builder;
    private String textElemForAssert;

    public GeneralActions(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

/**Logs in to Admin Panel. @param login @param password*/
    public void login(String login, String password) {
        // TODO implement logging in to Admin Panel
        driver.get(Properties.getBaseAdminUrl());
        driver.findElement(By.id("email")).sendKeys(login);
        driver.findElement(By.id("passwd")).sendKeys(password);
        driver.findElement(By.name("submitLogin")).click();

        throw new UnsupportedOperationException();
    }

    public void logout() {
        driver.get(Properties.getBaseAdminUrl());
        wait.until(ExpectedConditions.elementToBeClickable(By.id("header_employee_box")));
        driver.findElement(By.id("header_employee_box")).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.id("header_logout")));
        BaseTest.sleep(500);
        driver.findElement(By.id("header_logout")).click();

        throw new UnsupportedOperationException();
    }

    public void createProduct(ProductData newProduct) {
        // TODO implement product creation scenario
        builder = new Actions(driver);
/**двигаем курсор к нужному пункту меню*/
        BaseTest.sleep(2000); /**необходимо для повышения вероятности корректной отработки FireFox*/
        waitForContentLoad();
        builder.moveToElement(driver.findElement(By.id("subtab-AdminCatalog"))).build().perform();
/**кликаем на пункт сабменю и открываем необходимую страницу*/
        wait.until(ExpectedConditions.elementToBeClickable(By.id("subtab-AdminProducts")));
        driver.findElement(By.id("subtab-AdminProducts")).click();
/**кликаем на ссылку "+ новый продукт"*/
        wait.until(ExpectedConditions.elementToBeClickable(By.id("page-header-desc-configuration-add")));
        driver.findElement(By.id("page-header-desc-configuration-add")).click();
/**вводим имя нового продукта*/
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("form_step1_name_1")));
        driver.findElement(By.id("form_step1_name_1")).sendKeys(newProduct.getName());
/**вводим количество нового продукта*/
        driver.findElement(By.id("form_step1_qty_0_shortcut")).clear();
        driver.findElement(By.id("form_step1_qty_0_shortcut")).sendKeys(newProduct.getQty().toString());
/**вводим цену нового продукта*/
        driver.findElement(By.id("form_step1_price_shortcut")).clear();
        driver.findElement(By.id("form_step1_price_shortcut")).sendKeys(newProduct.getPrice());

/**Кликаем переключатель в нижней панели*/
        driver.findElement(By.xpath("//*[@id=\"form\"]//div[@class=\"switch-input \"]")).click();
/**проверяем содержимое предупреждающего сообщения на успешное завершение операции*/
        BaseTest.sleep(1000);
        try {
            textElemForAssert = driver.findElement(By.xpath("//div[@id='growls']//div[@class='growl-message']")).getText();
            Assert.assertEquals(textElemForAssert, "Настройки обновлены.");
            System.out.println("\033[32;1mChecking the message about the product creation success - \033[36;1mpassed\033[0m");
            CustomReporter.logAction("Checking the message about the product creation success - passed");
        } catch (AssertionError assErrE) {
            System.out.println("\033[32;1mChecking the message about the product creation success - \033[31;1mfailed\033[0m");
            CustomReporter.logAction("Checking the message about the product creation success - failed");
            System.out.println(assErrE);
        }
        BaseTest.sleep(200);
/**Закрываем pop-up сообщение*/
        driver.findElement(By.xpath("//div[@id='growls']//div[@class='growl-close']")).click();

        BaseTest.sleep(1000);

/**Кликаем "Сохранить" новый продукт*/
        driver.findElement(By.xpath("//div[@class='btn-group hide dropdown pull-right']/button[@class='btn btn-primary js-btn-save']")).click();
/**проверяем содержимое предупреждающего сообщения на успешное завершение операции*/
        BaseTest.sleep(1000);
        try {
            textElemForAssert = driver.findElement(By.xpath("//div[@id='growls']//div[@class='growl-message']")).getText();
            Assert.assertEquals(textElemForAssert, "Настройки обновлены.");
            System.out.println("\033[32;1mChecking the message about the product saving success - \033[36;1mpassed\033[0m");
            CustomReporter.logAction("Checking the message about the product saving success - passed");
        } catch (AssertionError assErrE) {
            System.out.println("\033[32;1mChecking the message about the product saving success - \033[31;1mfailed\033[0m");
            CustomReporter.logAction("Checking the message about the product saving success - failed");
            System.out.println(assErrE);
        }
        BaseTest.sleep(200);
/**Закрываем pop-up сообщение*/
        driver.findElement(By.xpath("//div[@id='growls']//div[@class='growl-close']")).click();

        throw new UnsupportedOperationException();
    }

    public void checkProduct(ProductData newProduct) {

/**открываем главную страницу магазина*/
        driver.get(Properties.getBaseUrl());
/**переходим по ссылке "все товары"*/
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@id=\"content\"]/section/a")));
        driver.findElement(By.xpath("//section[@id=\"content\"]/section/a")).click();
        waitForContentLoad();
/**вводим название нового товара в поле поиска и кликаем кнопку "поиск"*/
        driver.findElement(By.xpath("//div[@id=\"search_widget\"]//input[@class=\"ui-autocomplete-input\"]")).sendKeys(newProduct.getName());
        driver.findElement(By.xpath("//div[@id=\"search_widget\"]//i[@class=\"material-icons search\"]")).click();
        waitForContentLoad();
/**проверяем соответствие имени найденого товара имени созданного нового товара*/
        try {
            textElemForAssert = driver.findElement(By.xpath("//div[@id=\"js-product-list\"]/div[1]/article/div/div[1]/h1/a")).getText();
            Assert.assertEquals(textElemForAssert, newProduct.getName());
            System.out.println("\033[32;1mChecking the new product presence on the page - \033[36;1mpassed\033[0m");
            CustomReporter.logAction("Checking the new product presence on the page - passed");
        } catch (AssertionError assErrE) {
            System.out.println("\033[32;1mChecking the new product presence on the page - \033[31;1mfailed\033[0m");
            CustomReporter.logAction("Checking the new product presence on the page - failed");
            System.out.println(assErrE);
        }
/**открываем продукт*/
        driver.findElement(By.xpath("//div[@id=\"js-product-list\"]/div[1]/article/div/div[1]/h1/a")).click();
        waitForContentLoad();
/**проверяем соответствие имени продукта имени созданного нового товара*/
        try {
            textElemForAssert = driver.findElement(By.xpath("//section[@id=\"main\"]//h1[@class=\"h1\"]")).getText();
            Assert.assertEquals(textElemForAssert, newProduct.getName().toUpperCase());
            System.out.println("\033[32;1mChecking the new product name correct - \033[36;1mpassed\033[0m");
            CustomReporter.logAction("Checking the new product name correct - passed");
        } catch (AssertionError assErrE) {
            System.out.println("\033[32;1mChecking the new product name correct - \033[31;1mfailed\033[0m");
            CustomReporter.logAction("Checking the new product name correct - failed");
            System.out.println(assErrE);
        }
/**проверяем соответствие цены продукта цене созданного нового товара*/
        try {
            textElemForAssert = driver.findElement(By.xpath("//section[@id=\"main\"]//div[@class=\"current-price\"]//span")).getText();
            Assert.assertEquals(textElemForAssert.substring(0, textElemForAssert.length()-2), newProduct.getPrice());
            System.out.println("\033[32;1mChecking the new product price correct - \033[36;1mpassed\033[0m");
            CustomReporter.logAction("Checking the new product price correct - passed");
        } catch (AssertionError assErrE) {
            System.out.println("\033[32;1mChecking the new product price correct - \033[31;1mfailed\033[0m");
            CustomReporter.logAction("Checking the new product price correct - failed");
            System.out.println(assErrE);
        }
/**проверяем соответствие количества продукта количеству созданного нового товара*/
        try {
            textElemForAssert = driver.findElement(By.xpath("//div[@id=\"product-details\"]/div[@class=\"product-quantities\"]/span")).getText();
            Assert.assertEquals(textElemForAssert.substring(0, textElemForAssert.length()-4), newProduct.getQty().toString());
            System.out.println("\033[32;1mChecking the new product quantity correct - \033[36;1mpassed\033[0m");
            CustomReporter.logAction("Checking the new product quantity correct - passed");
        } catch (AssertionError assErrE) {
            System.out.println("\033[32;1mChecking the new product quantity correct - \033[31;1mfailed\033[0m");
            CustomReporter.logAction("Checking the new product quantity correct - failed");
            System.out.println(assErrE);
        }

        throw new UnsupportedOperationException();
    }

/**Waits until page loader disappears from the page*/
    public void waitForContentLoad() {
        // TODO implement generic method to wait until page content is loaded
        wait.until(driver1 -> ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete"));
    }
}
